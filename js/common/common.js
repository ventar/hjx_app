﻿// 首页登录弹窗
function pupopen() {
  $("#bg").show();
  $("#popbox").show();
  $("body").css("overflow", "hidden");
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
}

function pupclose() {
  $("#bg").hide();
  $("#popbox").hide();
  $("#popbox2").hide();
  $("body").css("overflow", "");
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}
$(document).on("click", "#popbox1 .confirm", function () {
  if ($("#grade").val() == "") {
    $(".tip1").css("opacity", "1");
    $(".tip2").css("opacity", "0");
  } else {
    if ($("#class").val() == "") {
      $(".tip1").css("opacity", "0");
      $(".tip2").css("opacity", "1");
    } else {
      $("#popbox1").hide()
      $("#popbox2").show();
    }
  }
})

$(".institution_option").click(function () {
  $(".login_nav dl.school_selects").addClass("is-show");
});
$(document).on("click", ".grade", function () {
  $(".login_nav dl.grade_selects").addClass("is-show");
  $(".login_nav dl.class_selects").removeClass("is-show");
});
$(document).on("click", ".class", function () {
  $(".login_nav dl.class_selects").addClass("is-show");
  $(".login_nav dl.grade_selects").removeClass("is-show");
});
$(document).on("click", ".login_nav dl", function () {
  $(".login_nav dl").removeClass("is-show");
});
$(document).on("click", "#popbox1 .grade_selects a", function () {
  document.getElementById("grade").value = this.text;
})
$(document).on("click", "#popbox1 .class_selects a", function () {
  document.getElementById("class").value = this.text;
})